RSTFILES=$(wildcard *.rst)
RSTFILES:=$(filter-out README.rst, $(RSTFILES))
OUTFILES=$(patsubst %.rst,public/%.html,$(RSTFILES))

IMGFILES=$(wildcard images/*)
OUTIMGFILES=$(patsubst %, public/%,$(IMGFILES))

all: $(OUTFILES) $(OUTIMGFILES)

public:
	mkdir -p public

public/images:
	mkdir -p public/images

public/%.html: %.rst | public
	ntdocutils -T mdl $< $@

public/images/%: images/% | public/images
	cp $< $@

clean:
	rm -rf public

.PHONY: all clean
