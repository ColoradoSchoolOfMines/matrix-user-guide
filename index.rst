Welcome to Matrix
#################

Joining Mines ACM on Matrix
===========================

Welcome to the Mines ACM tutorial on joining us on Matrix. I'm Robby, (former)
Vice Chair of Mines ACM and I'll be your guide through this adventure!

**If you're just interested in getting to the tutorial part of this (the TL;DR)
then let's** `Tutorial`_

If you run into something that seems wrong or doesn't make sense, feel free
to get my attention via Email, Matrix or by throwing a rock at me when
you see me on campus (which will likely result in me falling off my skateboard)
and I will definitely try to help you out.

About Matrix
------------
Matrix is, to quote their website:

    ...an open standard for interoperable, decentralised, real-time
    communication over IP. It can be used to power Instant Messaging,
    VoIP/WebRTC signaling, Internet of Things communication-- 
    or anywhere you need a standard HTTP API for publishing and subscribing to 
    data whilst tracking the conversation history.

If you're not crazy like me and Sumner and don't spend 50% of your life trying
out new forms of communication (read: bad chat apps), that probably doesn't mean
a ton to you. More likely it probably looks like it's full of more hipster
buzzwords then the average Medium post.

I'll try to give you my elevator pitch on our usage Matrix to help clear up
any confusion:

    Matrix is a communication platform in the same vein as Discord and Slack.
    What makes Matrix unique over these two other platforms is that it is
    federated. This means that while there are many places and ways you can
    connect to the Matrix network, if configured correctly, you should be able
    to communicate with any other user.

    Matrix is also a specification, not a project. This means that the
    information needed to write a matrix server or client exists freely on the
    internet for anyone to read, use, or contribute back to.

    Matrix does support the maintenance of a reference implementation of a
    server and client, both of which we will be interacting with today.

Maybe that pitch was better suited for more of a Top of The Rock [#]_ [#]_ style
elevator rather then the Brown Building [#]_ one. Hopefully I didn't bore you
too much and you'll continue on with me to the tutorial part of this.

.. [#] https://www.great-towers.com/towers/top-of-the-rock/

.. [#] If my references are lost on you it's probably because I'm letting my
       New York roots show

.. [#] Now that I think about it, the elevator in Brown is so crap thit it
       might actually be slower then the Top Of The Rock's 70 story climb

.. _tutorial:

Tutorial
========

Time for the meat and potatoes of this.

Joining Matrix isn't terribly hard, but with a little guidance I think that your
experience will be quite a bit better.

The first step in this process is...

Making An Account
-----------------

To join a Matrix channel or community you first have to, go figure, make an
account.

To do this, we are going to make use of Element, the most full featured Matrix
client.

Let's do that:

1. **Open the Element.io** `registration page`__.

   __ https://app.element.io/#/register

   .. image:: images/element-registration.png

2. **Fill in your desired information. Make sure to leave the Default server
   radio button seleted.**

   **If you don't give either your email or phone number you will be unable to
   reset your password in the event you forget / lost it.**

   **If you enter your phone number, don't forget to set the country code
   correctly**

   .. image:: images/element-registration-with-info.png

3. **If you entered a phone number, Element will send you a text message and ask
   you to enter the code it contained.**

   **If you entered a email, Element will send you an email and in that email
   will be a link you have to click.**

4. **After clicking the link in the email you will be asked to accept the terms
   and conditions. This is required.**

   **After accepting, you can close the tab.**

5. **Your Matrix account is all set now! Head over to our community on**
   `element.io`__ **to continue.**

   .. __: https://app.element.io/#/group/+mines-acm:matrix.org

6. **Some tips that I think will be useful**

   **If you find the notification frequency of a matrix room to be too much,
   you can change it on a per room basis.**

   **To change it on mobile (at least Android), just go to the Element.io
   home screen and long press the approprate room. Then change the
   options accordingly**

   .. image:: images/riot-android-room-prefs.png

   **To change it on desktop or browser, right click the room in the
   Element.im home page**

   .. image:: images/riot-desktop-or-browser-room-prefs.png
